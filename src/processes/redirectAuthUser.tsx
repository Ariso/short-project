import React, { useEffect, useState } from 'react'
import { Redirect } from 'react-router'
import { getFromLocalStorage } from '@app/lib'

export const RedirectAuthUser: React.FC = ({ children }) => {
	const [isAuth, setAuth] = useState(false)

	useEffect(() => {
		const token = getFromLocalStorage('token')
		setAuth(Boolean(token))
	}, [])

	if (isAuth) return <Redirect to='/profile' />
	return <>{children}</>
}
