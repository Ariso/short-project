import React, { useEffect, useState } from 'react'
import { Redirect } from 'react-router'
import { getFromLocalStorage } from '@app/lib'

export const RedirectUnAuthUser: React.FC = ({ children }) => {
	const [isAuth, setAuth] = useState(true)

	useEffect(() => {
		const token = getFromLocalStorage('token')
		setAuth(Boolean(token))
	}, [])

	if (!isAuth) return <Redirect to='/auth/login' />
	return <>{children}</>
}
