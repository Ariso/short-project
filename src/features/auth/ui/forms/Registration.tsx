import React from 'react'
import { Formik, Form, Field } from 'formik'
import styled from 'styled-components'
import { FieldProps } from 'formik/dist/Field'
import { theme } from '@app/styles'
import {
	CheckSecurityString,
	PasswordField,
	Button,
	Error,
	LinkButton,
	TextInput,
	TextLabel,
	BasicForm,
} from '@app/ui'
import { RegistrationValidationSchema } from '@app/features/auth/lib'
import { RegistrationPayload } from '@app/features/auth/interfaces'
import { OneParameterFunctionHandler } from '@app/interface'

interface PropsInterface {
	onSubmit: OneParameterFunctionHandler<RegistrationPayload>
	error?: string
}
export const RegistrationForm = ({ onSubmit, error }: PropsInterface) => {
	return (
		<BasicForm
			title='Sign up'
			message='Already have an account?'
			linkButton={<LinkButton to='/auth/login'>Authorization</LinkButton>}
		>
			<Formik
				initialValues={{
					name: '',
					email: '',
					phone: '',
					password: '',
					confirmPassword: '',
				}}
				validationSchema={RegistrationValidationSchema}
				onSubmit={onSubmit}
				enableReinitialize
			>
				{({ values: { password } }) => (
					<Form>
						<Fields>
							<Field name='name'>
								{({ field, meta }: FieldProps) => (
									<FieldWrapper>
										<TextLabel>Name</TextLabel>
										<TextInput placeholder='Name' {...field} />
										{meta.touched && meta.error && <Error>{meta.error}</Error>}
									</FieldWrapper>
								)}
							</Field>{' '}
							<Field name='phone'>
								{({ field, meta }: FieldProps) => (
									<FieldWrapper>
										<TextLabel>Phone</TextLabel>
										<TextInput placeholder='Phone' {...field} />
										{meta.touched && meta.error && <Error>{meta.error}</Error>}
									</FieldWrapper>
								)}
							</Field>{' '}
							<Field name='email'>
								{({ field, meta }: FieldProps) => (
									<FieldWrapper>
										<TextLabel>Email</TextLabel>
										<TextInput placeholder='Email' {...field} />
										{meta.touched && meta.error && <Error>{meta.error}</Error>}
									</FieldWrapper>
								)}
							</Field>
							<Field name='password'>
								{({ field, meta }: FieldProps) => (
									<FieldWrapper>
										<TextLabel>Password</TextLabel>
										<PasswordField placeholder='Password' {...field} />
										{meta.touched && meta.error && <Error>{meta.error}</Error>}
									</FieldWrapper>
								)}
							</Field>{' '}
							<Field name='confirmPassword'>
								{({ field, meta }: FieldProps) => (
									<FieldWrapper>
										<TextLabel>Repeat password</TextLabel>
										<PasswordField placeholder='Repeat password' {...field} />
										{meta.touched && meta.error && <Error>{meta.error}</Error>}
									</FieldWrapper>
								)}
							</Field>{' '}
							{error && <Error>{error}</Error>}
						</Fields>
						{password && <CheckSecurityString value={password} />}
						<Actions>
							<Button type='submit'>Submit</Button>
						</Actions>
					</Form>
				)}
			</Formik>
		</BasicForm>
	)
}

const Actions = styled.div`
	margin-top: 20px;
	button {
		font-family: 'Roboto', sans-serif;
		padding: 10px 0;
		font-size: 18px;
		font-weight: 500;
	}

	${theme.breakpoints.mobile} {
		display: flex;
		button {
			margin: 0 auto;
			width: max-content;
			padding: 10px 80px;
			border-right: 21px;
		}
	}
`
const Fields = styled.div`
	display: grid;
	grid-row-gap: 20px;
	margin-bottom: 20px;
`
const FieldWrapper = styled.div`
	.text_label {
		margin-bottom: 5px;
		margin-left: 15px;
		display: block;
	}
`
