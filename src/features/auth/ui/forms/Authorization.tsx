import React from 'react'
import { Formik, Form, Field } from 'formik'
import styled from 'styled-components'
import { FieldProps } from 'formik/dist/Field'
import { AuthorizationValidationSchema } from '@app/features/auth/lib'
import { OneParameterFunctionHandler } from '@app/interface'
import {
	TextInput,
	TextLabel,
	PasswordField,
	Error,
	Button,
	LinkButton,
	BasicForm,
} from '@app/ui'
import { AuthorizationPayload } from '@app/features/auth/interfaces'
import { theme } from '@app/styles'

interface PropsInterface {
	onSubmit: OneParameterFunctionHandler<AuthorizationPayload>
	error?: string
}
export const AuthorizationForm = ({ onSubmit, error }: PropsInterface) => {
	return (
		<BasicForm
			title='Log in'
			message={`Don't have an account?`}
			linkButton={<LinkButton to='/registration'>Registration</LinkButton>}
		>
			<Formik
				initialValues={{ email: '', password: '' }}
				validationSchema={AuthorizationValidationSchema}
				onSubmit={onSubmit}
			>
				{() => (
					<Form>
						<Fields>
							<Field name='email'>
								{({ field, meta }: FieldProps) => (
									<FieldWrapper>
										<TextLabel>Email</TextLabel>
										<TextInput placeholder='Email' {...field} />
										{meta.touched && meta.error && <Error>{meta.error}</Error>}
									</FieldWrapper>
								)}
							</Field>{' '}
							<Field name='password'>
								{({ field, meta }: FieldProps) => (
									<FieldWrapper>
										<TextLabel>Password</TextLabel>
										<PasswordField placeholder='Password' {...field} />
										{meta.touched && meta.error && <Error>{meta.error}</Error>}
									</FieldWrapper>
								)}
							</Field>{' '}
							{error && <Error>{error}</Error>}
						</Fields>
						<Actions>
							<Button type='submit'>Submit</Button>
						</Actions>
					</Form>
				)}
			</Formik>
		</BasicForm>
	)
}

const Actions = styled.div`
	margin-top: 20px;
	button {
		font-family: 'Roboto', sans-serif;
		padding: 10px 0;
		font-size: 18px;
		font-weight: 500;
	}

	${theme.breakpoints.mobile} {
		display: grid;
		button {
			margin: 0 auto;
			width: max-content;
			padding: 10px 80px;
			border-right: 21px;
		}
	}
`
const Fields = styled.div`
	display: grid;
	grid-row-gap: 20px;
`
const FieldWrapper = styled.div`
	.text_label {
		margin-bottom: 5px;
		margin-left: 15px;
		display: block;
	}
`
