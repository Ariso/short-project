import React from 'react'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router'
import { Button } from '@app/ui'
import { authActions } from './model'

export const Logout = () => {
	const dispatch = useDispatch()
	const history = useHistory()
	const onLogout = () => {
		dispatch(authActions.logOut({ history }))
	}

	return <Button onClick={onLogout}>Logout</Button>
}
