import React from 'react'
import styled from 'styled-components'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router'
import { theme } from '@app/styles'
import { Preloader } from '@app/ui'
import { authActions, authSelectors } from './model'
import { AuthorizationForm } from './ui'

import { AuthorizationPayload } from './interfaces'

export const Authorization = () => {
	const history = useHistory()
	const dispatch = useDispatch()
	const isLoading = useSelector(authSelectors.isLoading)
	const error = useSelector(authSelectors.error)

	const onSubmit = (data: AuthorizationPayload) => {
		dispatch(authActions.logIn({ ...data, history }))
	}

	return !isLoading ? (
		<Component>
			<AuthorizationForm onSubmit={onSubmit} error={error} />
		</Component>
	) : (
		<Preloader />
	)
}

const Component = styled.div`
	margin: 0 auto;
	${theme.breakpoints.mobile} {
		margin: 0;
	}
`
