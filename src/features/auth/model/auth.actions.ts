﻿import { HistoryInterface, InferActionsType } from '@app/interface'
import { AuthorizationPayload, RegistrationPayload } from '../interfaces'
import * as types from './auth.types'

export const actions = {
	toggleLoading: (payload: boolean) =>
		({
			type: types.TOGGLE_LOADING,
			payload,
		} as const),
	logIn: (payload: AuthorizationPayload & HistoryInterface) =>
		({
			type: types.LOG_IN,
			payload,
		} as const),
	registration: (payload: RegistrationPayload & HistoryInterface) =>
		({
			type: types.SIGN_UP,
			payload,
		} as const),
	logOut: (payload: HistoryInterface) =>
		({
			type: types.LOG_OUT,
			payload,
		} as const),
	setToken: (payload: string) =>
		({
			type: types.SET_TOKEN,
			payload,
		} as const),
	setError: (payload: string) =>
		({
			type: types.SET_ERROR,
			payload,
		} as const),
}

export type ActionsType = InferActionsType<typeof actions>
