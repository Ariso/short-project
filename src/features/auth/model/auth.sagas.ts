﻿import { all, takeEvery, put, call } from 'redux-saga/effects'
import { BasicPayloadInterface, HistoryInterface } from '@app/interface'
import { api } from '@app/api'
import { ErrorsEnum } from '@app/enums'
import { setAuthData } from '@app/features/auth/lib/setAuthData'
import { AuthorizationPayload, RegistrationPayload } from '../interfaces'
import { clearAuthData } from '../lib'
import * as types from './auth.types'
import { actions } from './auth.actions'

function* signUp({
	payload,
}: BasicPayloadInterface<RegistrationPayload & HistoryInterface>) {
	yield put(actions.toggleLoading(true))
	const { history, ...rest } = payload
	try {
		yield call(api.auth.registration, rest)
		history.push('/authorization')
		yield put(actions.toggleLoading(false))
	} catch (error) {
		const response = error.response.data.errors
		if (response.email?.includes(ErrorsEnum.EMAIL_ALREADY_EXIST)) {
			yield put(actions.setError(ErrorsEnum.EMAIL_ALREADY_EXIST))
		} else if (response.phone?.includes(ErrorsEnum.PHONE_ALREADY_EXIST)) {
			yield put(actions.setError(ErrorsEnum.PHONE_ALREADY_EXIST))
		}
		console.log(response)
	}
	yield put(actions.toggleLoading(false))
}

function* logIn({
	payload,
}: BasicPayloadInterface<AuthorizationPayload & HistoryInterface>) {
	yield put(actions.toggleLoading(true))
	const { history, ...rest } = payload
	try {
		const { meta } = yield call(api.auth.authorization, rest)
		const token = meta.custom.access_token
		setAuthData(token)
		history.push('/profile')
		yield put(actions.toggleLoading(false))
	} catch (error) {
		const response = error.response.data.errors
		if (response.email?.includes(ErrorsEnum.EMAIL_IS_INCORRECT)) {
			yield put(actions.setError(ErrorsEnum.EMAIL_IS_INCORRECT))
		}
	}
	yield put(actions.toggleLoading(false))
}
function* logout({ payload }: BasicPayloadInterface<HistoryInterface>) {
	yield put(actions.setToken(''))
	clearAuthData()
	payload.history.push('/authorization')
}

export function* rootSaga() {
	yield all([
		takeEvery(types.SIGN_UP, signUp),
		takeEvery(types.LOG_IN, logIn),
		takeEvery(types.LOG_OUT, logout),
	])
}
