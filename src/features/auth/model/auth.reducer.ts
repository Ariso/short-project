﻿import * as types from './auth.types'
import { ActionsType } from './auth.actions'

const initialState = {
	isLoading: false,
	token: null as null | string,
	error: '',
}

type InitialStateType = typeof initialState
export const reducer = (state = initialState, action: ActionsType): InitialStateType => {
	switch (action.type) {
		case types.TOGGLE_LOADING:
			return {
				...state,
				isLoading: action.payload,
			}
		case types.SET_TOKEN:
			return {
				...state,
				token: action.payload,
			}
		case types.SET_ERROR:
			return {
				...state,
				error: action.payload,
			}
		default:
			return state
	}
}
