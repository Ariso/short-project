import { AppStateType } from '@app/app/store'

export const isLoading = (state: AppStateType) => state.auth.isLoading

export const token = (state: AppStateType) => state.auth.token

export const error = (state: AppStateType) => state.auth.error
