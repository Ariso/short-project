export interface RegistrationPayload {
	name: string
	email: string
	password: string
	phone: string
}
