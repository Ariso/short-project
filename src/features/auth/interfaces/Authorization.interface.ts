export interface AuthorizationPayload {
	email: string
	password: string
}
