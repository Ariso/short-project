import { setToLocalStorage } from '@app/lib'

export const setAuthData = (token: string) => {
	setToLocalStorage('token', token)
}
