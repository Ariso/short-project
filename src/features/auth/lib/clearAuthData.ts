import { removeFromLocalStorage } from '@app/lib'

export const clearAuthData = () => {
	removeFromLocalStorage('token')
}
