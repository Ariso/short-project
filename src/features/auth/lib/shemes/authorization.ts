import * as Yup from 'yup'

const emailRegExp =
	/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
export const AuthorizationValidationSchema = Yup.object().shape({
	email: Yup.string().matches(emailRegExp, 'Email is invalid').required('Required'),
	password: Yup.string().min(2, 'Password too short').required('Required'),
})
