import * as Yup from 'yup'

const emailRegExp =
	/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
export const RegistrationValidationSchema = Yup.object().shape({
	phone: Yup.string().min(2, 'Phone is too short').required('Required'),
	email: Yup.string().matches(emailRegExp, 'Email is invalid').required('Required'),
	password: Yup.string().min(2, 'Password is too short').required('Required'),
	confirmPassword: Yup.string()
		.when('password', {
			is: (value: string) => Boolean(value && value.length > 0),
			then: Yup.string().oneOf([Yup.ref('password')], 'Both password need to be similar'),
		})
		.required('Required'),
})
