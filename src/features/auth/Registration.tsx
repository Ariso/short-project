import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router'
import styled from 'styled-components'
import { Preloader } from '@app/ui'
import { theme } from '@app/styles'
import { RegistrationPayload } from '@app/features/auth/interfaces'
import { RegistrationForm } from './ui'
import { authActions, authSelectors } from './model'

export const Registration = () => {
	const history = useHistory()
	const dispatch = useDispatch()
	const isLoading = useSelector(authSelectors.isLoading)
	const error = useSelector(authSelectors.error)

	const onSubmit = (data: RegistrationPayload) => {
		dispatch(authActions.registration({ ...data, history }))
	}

	return !isLoading ? (
		<Component>
			<RegistrationForm onSubmit={onSubmit} error={error} />
		</Component>
	) : (
		<Preloader />
	)
}

const Component = styled.div`
	margin: 0 auto;

	${theme.breakpoints.mobile} {
		margin: 0;
	}
`
