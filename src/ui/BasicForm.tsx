import React from 'react'
import styled from 'styled-components'
import { theme } from '@app/styles'

interface PropsTypes {
	title: string
	message?: string | null
	linkButton?: React.ReactNode
}
export const BasicForm: React.FC<PropsTypes> = ({
	children,
	title,
	message,
	linkButton,
}) => {
	return (
		<Component>
			<Title>{title}</Title>
			<Container>{children}</Container>
			{(message || linkButton) && (
				<Footer>
					{message && <Message>{message}</Message>}
					{linkButton}
				</Footer>
			)}
		</Component>
	)
}

const Container = styled.div`
	padding: 20px 40px 29px;
	${theme.breakpoints.mobile} {
		padding: 15px 5px 15px;
	}
`
const Title = styled.h1`
	text-align: center;
	font-size: 28px;
	font-weight: 700;
	color: ${({ theme }) => theme.pallet.dark_grey};
	background: ${({ theme }) => theme.pallet.white_2};
	padding: 24px 0 32px;
	${theme.breakpoints.mobile} {
		font-size: 25px;
		padding: 28px 0 28px;
	}
`
const Message = styled.div`
	font-family: 'Robot', sans-serif;
	color: ${({ theme }) => theme.pallet.grey};
`
const Footer = styled.div`
	background: ${({ theme }) => theme.pallet.white_2};
	display: grid;
	grid-template-columns: repeat(2, max-content);
	justify-content: space-between;
	padding: 20px 40px 25px;
	align-items: center;
	${theme.breakpoints.mobile} {
		grid-row-gap: 15px;
		justify-content: center;
		grid-template-columns: max-content;
	}
`

const Component = styled.div`
	display: grid;
	background: ${({ theme }) => theme.pallet.white};
	width: 425px;
	#error {
		text-align: center;
	}

	${theme.breakpoints.mobile} {
		grid-row-gap: 17px;
	}

	${theme.breakpoints.mobileL} {
		grid-row-gap: 10px;
		width: 100%;
	}
`
