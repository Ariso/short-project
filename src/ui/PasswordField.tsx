import React, { ChangeEvent, useState } from 'react'
import styled from 'styled-components'

import { CloseGlassIcon, OpenGlassIcon } from '@app/images'
import { OneParameterFunctionHandler, FocusEvent } from '@app/interface'
import { TextInput } from './Input'

interface PropsTypes {
	value: string
	onChange: OneParameterFunctionHandler<ChangeEvent>
	onBlur?: FocusEvent
	onFocus?: VoidFunction
	placeholder?: string
	name?: string
}
export const PasswordField: React.FC<PropsTypes> = ({
	value,
	onChange,
	onFocus,
	placeholder,
	name,
}) => {
	const [isVisibleValue, setIsVisibleValue] = useState(false)
	const onToggleVisible = () => {
		setIsVisibleValue((prevState) => !prevState)
	}
	return (
		<Component>
			<TextInput
				onChange={onChange}
				value={value}
				onFocus={onFocus}
				type={isVisibleValue ? 'text' : 'password'}
				placeholder={placeholder}
				name={name}
			/>
			<Icon
				src={isVisibleValue ? OpenGlassIcon : CloseGlassIcon}
				onClick={onToggleVisible}
			/>
		</Component>
	)
}

const Component = styled.div`
	position: relative;
`
const Icon = styled.img`
	position: absolute;
	top: 50%;
	transform: translateY(-50%);
	right: 11px;
	cursor: pointer;
`
