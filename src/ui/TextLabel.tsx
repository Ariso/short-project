import React from 'react'
import styled from 'styled-components'

interface PropsTypes {
	fontSize?: string
}
export const TextLabel: React.FC<PropsTypes> = ({ children, fontSize = '14px' }) => {
	return (
		<Component fontSize={fontSize} className='text_label'>
			{children}
		</Component>
	)
}

interface ComponentInterface {
	fontSize: string
}
const Component = styled.span<ComponentInterface>`
	color: ${({ theme }) => theme.pallet.grey};
	font-family: 'Roboto', sans-serif;
	font-weight: 300;
	font-size: ${({ fontSize }) => fontSize};
`
