import React from 'react'
import styled from 'styled-components'
import { ButtonTypes } from '@app/interface'

interface PropsTypes {
	background?: string
	type?: ButtonTypes
	onClick?: VoidFunction
	disabled?: boolean
}
export const Button: React.FC<PropsTypes> = ({
	children,
	type = 'button',
	onClick,
	disabled,
}) => {
	return (
		<Component type={type} onClick={onClick} disabled={disabled}>
			{children}
		</Component>
	)
}

const Component = styled.button`
	background: ${({ theme }) => theme.pallet.green};
	font-family: 'Robot', sans-serif;
	color: #ffffff;
	font-weight: 500;
	cursor: pointer;
	font-size: 14px;
	transition: 0.2s linear;
	border-radius: 3px;
	width: 100%;
	&:disabled {
		background: ${({ theme }) => theme.pallet.green};
		&:hover {
			background: ${({ theme }) => theme.pallet.green};
		}
	}
	&:not(:disabled) {
		&:hover {
			opacity: 0.8;
		}
	}
`
