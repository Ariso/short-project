import React from 'react'
import styled from 'styled-components'
import { NavLink } from 'react-router-dom'
import { theme } from '@app/styles'

interface PropsTypes {
	to: string
}
export const LinkButton: React.FC<PropsTypes> = ({ to, children }) => {
	return <Button to={to}>{children}</Button>
}

const Button = styled(NavLink)`
	font-family: 'Roboto', sans-serif;
	background: ${({ theme }) => theme.pallet.grey};
	color: ${({ theme }) => theme.pallet.white};
	font-weight: 500;
	padding: 6px 20px;
	text-align: center;
	border-right: 3pxv ${theme.breakpoints.mobile} {
		text-align: center;
		border-radius: 35px;
	}
`
