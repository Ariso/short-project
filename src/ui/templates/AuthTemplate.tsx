import React from 'react'
import styled from 'styled-components'
import { AuthBackgroundImage } from '@app/images'

export const AuthTemplate: React.FC = ({ children }) => {
	return (
		<Template>
			<Container>{children}</Container>
		</Template>
	)
}

const Container = styled.div`
	display: grid;
	align-items: center;
	height: calc(100vh - 74px);
`
const Template = styled.div`
	background: #f5f5f5;
	background: url(${AuthBackgroundImage}) no-repeat center;
	background-size: cover;
	height: 100vh;
`
