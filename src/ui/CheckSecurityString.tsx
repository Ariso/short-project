import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { ObjectInterface } from '@app/interface'
import { theme } from '@app/styles'

interface PropsTypes {
	value: string
}

const colorsByStages: ObjectInterface<() => string> = {
	bad: () => theme.pallet.red,
	medium: () => theme.pallet.yellow,
	good: () => theme.pallet.green,
}
export const CheckSecurityString: React.FC<PropsTypes> = ({ value }) => {
	const [stageName, setStageName] = useState('Bad')
	useEffect(() => {
		if (value.length >= 8) setStageName('Medium')
		if (/[A-Z]/.test(value) && value.length >= 8 && /[0-9]/.test(value))
			setStageName('Good')
	}, [value])

	return (
		<Component>
			<Stages>
				<Stage background={colorsByStages[stageName.toLowerCase()]()} />
				<Stage background={colorsByStages[stageName.toLowerCase()]()} />
				<Stage
					background={
						stageName === 'Medium' || stageName === 'Good'
							? colorsByStages[stageName.toLowerCase()]()
							: ''
					}
				/>
				<Stage
					background={
						stageName === 'Medium' || stageName === 'Good'
							? colorsByStages[stageName.toLowerCase()]()
							: ''
					}
				/>
				<Stage
					background={
						stageName === 'Good' ? colorsByStages[stageName.toLowerCase()]() : ''
					}
				/>
				<Stage
					background={
						stageName === 'Good' ? colorsByStages[stageName.toLowerCase()]() : ''
					}
				/>
			</Stages>
			<StageName color={colorsByStages[stageName.toLowerCase()]()}>{stageName}</StageName>
		</Component>
	)
}

interface StageInterface {
	color?: string
	background?: string
}

interface StageNameInterface {
	color?: string
}
const Component = styled.div`
	display: grid;
	grid-template-columns: 1fr max-content;
	align-items: center;
	justify-content: space-between;
`
const Stages = styled.div`
	display: grid;
	grid-template-columns: repeat(6, 25px);
	justify-content: space-between;

	${theme.breakpoints.mobileL} {
		grid-template-columns: repeat(6, 25px);
	}

	${theme.breakpoints.mobile} {
		grid-template-columns: repeat(6, 20px);
	}
`
const Stage = styled.div<StageInterface>`
	height: 3px;
	background: ${({ theme, background }) => background || theme.pallet.light_grey};
`
const StageName = styled.div<StageNameInterface>`
	margin-left: 16px;
	color: ${({ color }) => color};
`
