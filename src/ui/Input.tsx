import React from 'react'
import styled from 'styled-components'
import {
	Input,
	ChangeEvent,
	OneParameterFunctionHandler,
	ObjectInterface,
} from '@app/interface'

interface PropsTypes {
	disabled?: boolean
	onChange: OneParameterFunctionHandler<ChangeEvent>
	type?: Input
	autoComplete?: boolean
	styles?: ObjectInterface<string>
	maxLength?: number
	placeholder?: string
	className?: string
	id?: string
	onFocus?: VoidFunction
	componentRef?: React.MutableRefObject<HTMLInputElement | undefined>
	name?: string
	value: string
}

export const TextInput: React.FC<PropsTypes> = ({
	disabled = false,
	type = 'text',
	onChange,
	autoComplete = false,
	styles,
	maxLength,
	onFocus,
	componentRef,
	placeholder,
	className,
	id,
	name,
	value,
}) => {
	return (
		<Wrapper>
			<Component
				onChange={onChange}
				value={value}
				disabled={disabled}
				type={type}
				autoComplete={autoComplete.toString()}
				style={styles}
				maxLength={maxLength}
				onFocus={onFocus}
				ref={componentRef as React.RefObject<HTMLInputElement>}
				placeholder={placeholder}
				className={className}
				id={id}
				name={name}
			/>
		</Wrapper>
	)
}

interface BehaviorInterface {
	color?: string
}
const Component = styled.input<BehaviorInterface>`
	width: 100%;
	font-size: 16px;
	padding: 13px 0 13px 18px;
	text-align: left;
	color: ${({ theme }) => theme.pallet.black};
	border-right: 3px;
	border: 0;
	border-bottom: 1px solid ${({ theme }) => theme.pallet.light_grey};
	&:disabled {
		background: transparent;
	}

	&::placeholder {
		font-size: 16px;
		color: ${({ theme }) => theme.pallet.grey};
	}
`
const Wrapper = styled.div`
	position: relative;
`
const Image = styled.img`
	width: 21px;
	position: absolute;
	right: 15px;
	top: 50%;
	transform: translateY(-50%);
`
