import React from 'react'
import styled from 'styled-components'

export const Error: React.FC = ({ children }) => {
	return <Component>{children}</Component>
}

const Component = styled.span`
	color: ${({ theme }) => theme.pallet.red};
	font-size: 14px;
`
