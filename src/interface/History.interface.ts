import { RouteComponentProps } from 'react-router'

export interface HistoryInterface {
	history: RouteComponentProps['history']
}
