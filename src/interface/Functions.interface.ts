export type OneParameterFunctionHandler<T = string> = (value: T) => void
