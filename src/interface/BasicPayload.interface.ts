export interface BasicPayloadInterface<P> {
	type: string
	payload: P
}
