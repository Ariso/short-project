export interface ObjectInterface<T> {
	[key: string]: T
}
