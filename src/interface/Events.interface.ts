import React from 'react'

export type FormEvent = React.FormEvent<HTMLFormElement>
export type ChangeEvent<T = HTMLInputElement> = React.ChangeEvent<T>
export type FocusEvent = (e: React.FocusEvent<any>) => void
