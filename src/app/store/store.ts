import { applyMiddleware, createStore } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { rootReducer } from './root.reducer'
import { rootSaga } from './root.saga'

const sagaMiddleware = createSagaMiddleware()

export const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))

type RootReducerType = typeof rootReducer

sagaMiddleware.run(rootSaga)

export type AppStateType = ReturnType<RootReducerType>
