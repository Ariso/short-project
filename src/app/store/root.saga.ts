import { all } from 'redux-saga/effects'
import { authRootSaga } from '@app/features/auth'

export function* rootSaga() {
	yield all([authRootSaga()])
}
