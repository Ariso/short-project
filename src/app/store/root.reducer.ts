import { combineReducers } from 'redux'
import { authReducer } from '@app/features/auth'

export const rootReducer = combineReducers({ auth: authReducer })
