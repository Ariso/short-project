import React from 'react'
import styled from 'styled-components'
import { Redirect, Route, Switch } from 'react-router'
import { routes } from '@app/pages'

export const App = (): React.ReactElement => {
	return (
		<Application>
			<Switch>
				{routes.map(({ Component, exact, path }) => {
					return (
						<Route key={path} exact={exact} path={path} render={() => <Component />} />
					)
				})}
				<Redirect to='/authorization' />
			</Switch>
		</Application>
	)
}

const Application = styled.div``
