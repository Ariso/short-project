import React from 'react'
import { RedirectUnAuthUser } from '@app/processes'
import { Logout } from '@app/features'

export const ProfilePage = () => {
	return (
		<RedirectUnAuthUser>
			<Logout />
			<div>Here must be profile page after successful authorization</div>
		</RedirectUnAuthUser>
	)
}
