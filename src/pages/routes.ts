import { RegistrationPage } from './Registration'
import { AuthorizationPage } from './Authorization'
import { ProfilePage } from './Profile'

export const routes = [
	{
		path: '/profile',
		Component: ProfilePage,
		exact: true,
	},
	{
		path: '/authorization',
		Component: AuthorizationPage,
		exact: true,
	},
	{
		path: '/registration',
		Component: RegistrationPage,
		exact: true,
	},
]
