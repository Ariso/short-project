import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { Authorization, authActions } from '@app/features/auth'
import { RedirectAuthUser } from '@app/processes'
import { AuthTemplate } from '@app/ui/templates'

export const AuthorizationPage = () => {
	const dispatch = useDispatch()

	useEffect(() => {
		return () => {
			dispatch(authActions.setError(''))
		}
	}, [dispatch])

	return (
		<RedirectAuthUser>
			<AuthTemplate>
				<Authorization />
			</AuthTemplate>
		</RedirectAuthUser>
	)
}
