import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { RedirectAuthUser } from '@app/processes'
import { authActions } from '@app/features/auth'
import { AuthTemplate } from '@app/ui'
import { Registration } from '@app/features/auth/Registration'

export const RegistrationPage = () => {
	const dispatch = useDispatch()
	useEffect(() => {
		return () => {
			dispatch(authActions.setError(''))
		}
	}, [dispatch])

	return (
		<RedirectAuthUser>
			<AuthTemplate>
				<Registration />
			</AuthTemplate>
		</RedirectAuthUser>
	)
}
