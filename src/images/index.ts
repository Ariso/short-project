import CloseGlassIcon from './glass.svg'
import OpenGlassIcon from './open-glass.svg'
import AuthBackgroundImage from './auth-background.png'

export { CloseGlassIcon, OpenGlassIcon, AuthBackgroundImage }
