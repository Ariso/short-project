export enum ErrorsEnum {
	PHONE_ALREADY_EXIST = 'This phone already exists',
	EMAIL_ALREADY_EXIST = 'The email has already been taken.',
	EMAIL_IS_INCORRECT = 'Your email is incorrect',
}
