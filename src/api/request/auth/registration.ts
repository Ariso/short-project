import { request } from '@app/api/requrests'

export const registration = <T>(payload: T) =>
	request({ url: 'register', method: 'POST', data: payload })
