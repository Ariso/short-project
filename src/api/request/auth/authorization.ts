import { request } from '@app/api/requrests'

export const authorization = <T>(payload: T) =>
	request({ url: 'login', method: 'POST', data: payload })
