import axios, { AxiosRequestConfig } from 'axios'
import { getFromLocalStorage } from '@app/lib'

export const request = <T>(config: AxiosRequestConfig): Promise<T> => {
	let headers
	const token = getFromLocalStorage('token')

	if (token) headers = { Authorization: `Bearer ${token}` }

	const instance = axios.create({
		baseURL: process.env.BASE_URL || 'https://api.thesadovsky.pl/api/v1',
		headers,
	})

	return instance(config).then((response) => response.data)
}
